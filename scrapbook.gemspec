$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'scrapbook/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'scrapbook'
  s.version     = Scrapbook::VERSION
  s.authors     = ['Madeline Cowie']
  s.email       = ['madeline@cowie.me']
  s.homepage    = 'http://www.neotericdesign.com'
  s.summary     = 'Asset Management for the Neoteric Design CMS'
  s.description = 'Asset Management for the Neoteric Design CMS'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile',
                'README.md']

  s.add_dependency 'rails', '>= 5.0'
  s.add_dependency 'paperclip', '>= 4.3'
  s.add_dependency 'pg'

  s.add_dependency 'bourbon'
  s.add_dependency 'neat', '~> 1.7.4'
  s.add_dependency 'jquery-rails'

  s.add_development_dependency 'formtastic'
  s.add_development_dependency 'bitters', '~> 1.2.0'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'factory_girl_rails', '>= 4.7.0'
  s.add_development_dependency 'pry-rails'
  s.add_development_dependency 'rspec-rails', '>= 3.5.0'
  s.add_development_dependency 'simplecov'
end
