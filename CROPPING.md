# Cropping Workflow

# Concept

An application has many image Contexts, defining specific relationships between an image and its parent. A Context has a target resolution, defining the optimal resolution/aspect ratio for that usage.

# Workflow

1. Developer sets up Contexts for each type of image placement. Contexts include a 'target' resolution.
2. User is on a form for a parent model, uses the sub form for a context to browse the central image library.
3. User picks or uploads a new image.
4. User is presented with a cropper. The cropper is initialized with either the previous crop of that Placement, or the largest area that can be covered by the context's aspect ratio. The cropper is locked to that ratio, and maybe a minimum size [Context's target rez * 0.667 ?].
5. System takes the crop (or entire image if no crop) and generates at least the @1x pixel density file, @2x if the crop has enough pixels.
6. User is returned to the parent model form, with the 'field' filled with the cropped image.
7. On the front end, we have a special image tag helper that, given an image record, automatically generates the HTML tag with available srcset's

# Benefits of approach
Each Placement only stores and is concerned with its version(s) of the file. We can have many versions of a file, without worrrying where its been used before. And the Master record is not overloaded with all the croppings and sizings of those crops.

## Drawbacks
There can be some duplications of storage because each Placement generates its own files, so if a Master is used in the same context multiple times, those would generate new files (and specific metadata). This is still probably an edge case, and handling this is naively is definitely more comfortable for a version 1. For UX puporses, could consider looking up 'siblings' as  a starting point for working with a placement.

# Tech

Placement record stores the offset (top left corner) and size (WxH) of the crop, for its context. If no crop is set, use the full image; autocropping from the center, to the desired aspect ratio.

It _always_ generates the @1x pixel density version, so we can rely on having that available, scaling it up if need be. If the crop is big enough* generate a @2x retina image as well. Bonus to calculating all this ourselves before it hits Paperclip is that we don't have to muddle with its custom processors or internal workflow, and we're less intertwined with its fate.

*Big Enough formula pending