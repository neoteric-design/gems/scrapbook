module Scrapbook
  # View helpers for general parent application views
  module PublicHelper
    def scrapbook_image_tag(placement, **options)
      standard_url = placement&.file_url(:standard)
      return if standard_url.blank?

      default_options = {
        alt: placement.metadata['alt_text'],
        data: placement.metadata
      }
      if placement.crop_retina_ready?
        retina_url = placement.file_url(:retina)
        default_options[:srcset] = "#{retina_url} 2x"
      end
      image_tag standard_url, default_options.merge(options)
    end
  end
end
