module Scrapbook
  # View helpers for in-engine views
  module LibraryHelper
    def metadata_fields
      Scrapbook.config.metadata_fields.values
    end

    def freehand_cropping?
      current_context.target_resolution.width_only?
    end

    def free_crop_ratios
      [[3, 2], [4, 3], [5, 4], [16, 9], [1, 1]]
    end

    def current_context
      return unless context_source

      Context.find(context_source)
    end

    def hint_text(translation_name, context = current_context, **options)
      return unless placing?
      t_opts = { context: context.title,
                 file_type_category: context.file_type_category }.merge(options)
      translation = t(translation_name, t_opts)

      content_tag :p, translation, class: 'hint-text'
    end

    private

    def context_source
      return params[:context] if placing?
      return @placement.context_name if @placement
    end
  end
end
