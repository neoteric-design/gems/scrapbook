module Scrapbook
  class ApplicationController < ActionController::Base
    helper_method :placing?

    if Scrapbook.config.before_action
      before_action Scrapbook.config.before_action
    end

    def url_options
      { :context => params[:context] }.merge(super)
    end

    def placing?
      params[:context].present?
    end
  end
end
