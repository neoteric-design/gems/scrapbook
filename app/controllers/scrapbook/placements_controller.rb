require_dependency "scrapbook/application_controller"

module Scrapbook
  class PlacementsController < ApplicationController
    before_action :find_placement, only: [:show, :edit, :update, :destroy]

    def index
      @placements = Placement.all
    end

    def show
    end

    def new
      @master = Master.find(params[:master])
      @placement = Placement.new(context_name: params[:context])
    end

    def edit
      @master = @placement.master
    end

    def create
      @placement = Placement.new(placement_params)
      if @placement.save
        redirect_to placement_path(@placement),
                    notice: I18n.t('placements.saved')
      else
        render :new, error: I18n.t('errors.general')
      end
    end

    def update
      if @placement.update(placement_params.except(:context_name))
        redirect_to placement_path(@placement),
                    notice: I18n.t('placements.saved')
      else
        render :edit, error: I18n.t('errors.general')
      end
    end

    def destroy
      if @placement.destroy
        redirect_to placements_path, alert: I18n.t('placements.destroyed')
      else
        redirect_to @placement, error: I18n.t('errors.general')
      end
    end

    private
    def find_placement
      @placement = Placement.find(params[:id])
    end

    def placement_params
      params.require(:placement).permit(:title, :crop_origin, :crop_resolution,
        :scrapbook_master_id, :context_name, metadata_field_names)
    end

    def metadata_field_names
      Scrapbook.config.metadata_fields.keys
    end
  end
end
