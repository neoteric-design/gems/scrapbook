require_dependency "scrapbook/application_controller"

module Scrapbook
  class MastersController < ApplicationController
    before_action :find_master, only: %i[show edit update destroy]

    def index
      @masters = Master.all
    end

    def search
      @masters = Master.keyword_search(params[:q])
      render :index
    end

    def show; end

    def new
      @master = Master.new
    end

    def edit; end

    def create
      @master = Master.new(master_params)

      if @master.save
        destination = placing? ? new_placement_path(master: @master) : @master
        redirect_to destination, notice: I18n.t('masters.saved')
      else
        render :new, error: I18n.t('errors.general')
      end
    end

    def update
      if @master.update(master_params)
        redirect_to masters_path, notice: I18n.t('masters.saved')
      else
        render :edit, error: I18n.t('errors.general')
      end
    end

    def destroy
      if @master.destroy
        redirect_to masters_path, alert: I18n.t('masters.destroyed')
      else
        redirect_to @master, error: I18n.t('errors.general')
      end
    end

    private

    def find_master
      @master = Master.find(params[:id])
    end

    def master_params
      params.require(:master)
            .permit(:title, :file, :taken_on, metadata_field_names)
    end

    def metadata_field_names
      Scrapbook.config.metadata_fields.keys
    end
  end
end
