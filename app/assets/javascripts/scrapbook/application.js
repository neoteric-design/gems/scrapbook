//= require jquery
//= require jquery_ujs
//= require scrapbook/refills/tabs
//= require scrapbook/cropper
//= require scrapbook/placements
//= require scrapbook/masters

$(function(){
  tinymce.init({
    selector: 'textarea:not(.plaintext)',
    plugins: ["link"],
    menubar: false,
    toolbar: 'bold italic | link | removeformat',
    statusbar: false,
    valid_elements : 'a[href|target],strong/b,i/em'
  });
});