
var Scrapbook = function(selector) {
  return new Vue({
    el: selector
  });
};

initScrapbooks = function(selector){
  selector = selector ||  '.scrapbook.input';
  els = document.querySelectorAll(selector);

  window.scrapbooks = [];

  for(i = 0; i < els.length; i++) {
    window.scrapbooks.push(Scrapbook(els[i]));
  }
};