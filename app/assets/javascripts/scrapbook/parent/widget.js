//
// Requires Vue 2, vue-draggable (sortable.js in turn)
//

function scrapbookPlaceholder(placeholderPath, libraryPath){
 return {
    id: null,
    file_urls: { standard: placeholderPath },
    edit_path: libraryPath,
    title: "Waiting.",
    removed: false
  };
}

Vue.component('scrapbook-widget', {
  props: ['context', 'many', 'fieldname', 'initialPlacements', 'libraryPath', 'placeholderPath'],
  data: function() {
    data = this.initialPlacements;
    if(typeof(data) === 'undefined' || data.length === 0){
      // Can't access component's methods in the data function, thus duplication for now
      data = [ scrapbookPlaceholder(this.placeholderPath, this.libraryPath) ];
    }

    return { placements: data };
  },
  computed: {
    placementIds: function() {
      return this.placements
                   .filter(function(pl) { return (!pl.removed && pl.id !== null); })
                   .map(function(pl) { return pl.id; });
    }
  },
  methods: {
    add: function() {
      this.placements.push(this.placeholder());
    },
    placeholder: function() {
      return scrapbookPlaceholder(this.placeholderPath, this.libraryPath);
    },
    update: function(idx, placementData) {
      Vue.set(this.placements, idx, Object.assign(this.placements[idx], placementData));
    },
    remove: function(placement) {
      Vue.set(placement, 'removed', true);
    },
    unremove: function(placement){
      Vue.set(placement, 'removed', false);
    },
    edit: function(placement) {
      popup = window.open(placement.edit_path + "?context=" + this.context);
      this.editingPlacementIdx = this.placements.indexOf(placement);
      self = this;

      window.addEventListener('message', self.receiveData);
    },
    receiveData: function(event) {
      if (event.origin !== window.location.origin) {
        console.log("Origin does not match");
        return;
      }
      if(typeof(event.data.placement) === 'undefined'){
        console.log("No placement data found.");
        return;
      }

      window.removeEventListener('message', this.receiveData);
      this.update(this.editingPlacementIdx, event.data.placement);
      this.editingPlacementIdx = null;
    },
    imgClass: function(placement) {
      if(placement.id === null) {
        return "scrapbook-placeholder";
      } else {
        return "scrapbook-preview";
      }
    }
  },
  template: '<div class="scrapbook-widget">' +
'  <input type="hidden" :name="fieldname">' +
'  <input type="hidden" v-for="id in placementIds" :name="fieldname" :value="id">' +
'  <draggable :list="placements" :options="{ disabled: !many }">' +
'    <div class="scrapbook-placement"' +
'         v-for="(placement, index, key) in placements"' +
'         :index="index" :key="key"' +
'         :placement="placement" :context="context">' +
'      <div class="scrapbook-image" v-if="!placement.removed">' +
'        <img :class="imgClass(placement)" :src="placement.file_urls.standard" @click="edit(placement)">' +
'        <div class="info-controls" v-if="!(placement.id === null)">' +
'          <span>{{ placement.title }}</span>' +
'          <button @click.prevent="edit(placement)">Edit</button>' +
'          <button @click.prevent="remove(placement)">Remove</button>' +
'        </div>' +
'      </div>' +
'      <div class="scrapbook-removed" v-if="placement.removed">' +
'        <button @click.prevent="unremove(placement)">Undo remove</button>' +
'      </div>' +
'    </div>' +
'  </draggable>'+
'  <button v-if="many" v-on:click.prevent="add">Add</button>' +
'</div>'
});