var maximizeCropbox = function() {
  imageData = cropper.getImageData();
  console.log(imageData);
  cropper.setData({
    x: 0,
    y: 0,
    scale: 0,
    zoom: 1,
    width: imageData.naturalWidth,
    height: imageData.naturalHeight,
    rotate: 0
  });
};

var unlockCropbox = function() {
  cropper.setAspectRatio('NaN');
};

var rotateCropbox = function() {
  cropbox = cropper.getCropBoxData();
  setCropperRatio(cropbox.height, cropbox.width);
};

var setCropperRatio = function(x, y) {
  newRatio = x / y;
  cropper.setAspectRatio(newRatio);
  return(newRatio);
};



var notifyParent = function(placementData) {
  window.opener.postMessage(placementData, window.location.origin);
  window.opener.focus();
  window.close();
};

window.onload = function() {
  initCropper();
};

var initCropper = function() {
  var cropperImage = document.getElementById('cropper-image');

  if(cropperImage === null)
    return false;

  var initialCrop = JSON.parse(cropperImage.dataset.cropMetrics);
  var aspectRatio = cropperImage.dataset.aspectRatio;
  var inputOrigin = document.getElementById('placement_crop_origin');
  var inputResolution = document.getElementById('placement_crop_resolution');

  window.cropper = new Cropper(cropperImage, {
    data: initialCrop,
    aspectRatio: aspectRatio,
    zoomable: false,
    movable: false,
    crop: function(e) {
      var currentOrigin = e.detail.x + ',' + e.detail.y;
      var currentResolution = e.detail.width + 'x' + e.detail.height;
      inputOrigin.value = currentOrigin;
      inputResolution.value = currentResolution;
    },
    built: function() {
      if(initialCrop.height === 0) {
        maximizeCropbox();
      }
    }
  });

  return true;
};
