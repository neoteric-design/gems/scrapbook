module Scrapbook
  class Master < ApplicationRecord
    has_attached_file :file,
      styles: { large: '800x800>', square_thumb: '250x250#' }
    do_not_validate_attachment_file_type :file

    has_many :placements, dependent: :destroy,
                          foreign_key: :scrapbook_master_id,
                          after_add: :copy_file_to_placement

    before_save :extract_resolution
    attribute :image_resolution, :scrapbook_resolution

    def all_metadata
      attributes.select { |k, v| self.class.metadata_fields.include?(k) }
    end

    def metadata
      all_metadata.select { |k, v| v.present? }
    end

    def metadata=(hsh)
      assign_attributes(hsh)
    end

    def file=(attr)
      return if file.exists?
      file.assign(attr)
    end

    def copy_file_to_placement(placement)
      placement.file = file
    end

    def self.keyword_search(term, fields = searchable_fields)
      query_string = fields
                     .map { |fn| "#{fn}::varchar ILIKE :term" }
                     .join(' OR ')
      where(query_string, term: "%#{term}%")
    end

    def title
      self[:title].present? ? self[:title] : file_file_name
    end

    def image?
      file_content_type =~ %r{^(image|(x-)?application)/(bmp|gif|jpeg|jpg|pjpeg|png|x-png)$}
    end

    private

    def self.metadata_fields
      Scrapbook.config.metadata_fields.keys
    end

    def self.searchable_fields
      metadata_fields.prepend(:title)
    end

    def extract_resolution
      return if !image? || file.blank?
      tempfile = file.queued_for_write[:original]
      return if tempfile.nil?
      self.image_resolution = Paperclip::Geometry.from_file(tempfile).to_s
    end
  end
end
