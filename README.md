# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Scrapbook
Asset Management Library for the Neoteric Design CMS

## Installation

Neoteric CMS gems are served from a private host. Replace `SECURE_GEMHOST` with the source address.

```ruby
# Gemfile
source SECURE_GEMHOST do
  gem 'scrapbook'
end
```

```sh
$ bundle install

$ rails g scrapbook:install

$ rake db:migrate
```
## Usage

### Contexts

Scrapbook is built to attach assets to a parent model. The first step is to
identify these attachment points, and register them so Scrapbook knows about  them. Scrapbook's configuration block provides an easy and consolidated place to do this.

Contexts are required to have a name, and a target_resolution. Set the many flag to true to allow adding a collection of assets. Optionally provide a human-friendly label.

```ruby
# config/initializers/scrapbook.rb
  config.register_context :carousel_items, many: true, target_resolution: [800,0]
  config.register_context :hero, label: "Hero Image" target_resolution: [800,0]
```

### Parent model

Next, add the scrapbook to your model.

```ruby
class Page
  has_scrapbook :hero
end
```

Note, **contexts are meant to be reusable**. Say you have Pages, NewsPosts, and Events, all of which have a hero image. They can all share the same `hero` context. Define once, use anywhere!

### Interface

#### Assets

To use the scrapbook input widget, you'll need the following:

* VueJS 2.0+
* Sortable.js
* Vue-Draggable

For convenience these are packaged in a `dependencies` folder if you don't want or need to include them in a different way.


```coffeescript
# active_admin.js.coffee

#= require scrapbook/dependencies/vuejs-2.0.3.js
#= require scrapbook/dependencies/sortable.js
#= require scrapbook/dependencies/vue-draggable.js

#= require scrapbook/parents
```

Then, initialize the widget

```coffeescript
# active_admin.js.coffee


```


Also include the parents stylesheet to get lookin' nice.

```scss
// active_admin.css.scss

@import "scrapbook/parents";
```


#### Strong parameters

You'll need to whitelist the appropriate id parameters in your controller. These follow standard Rails conventions. Examle:

Singular: `:hero_id`

Many: `carousel_item_ids: []`


#### Formtastic

And the payoff for all that set up; all you have to do for each scrapbook is this:

```erb
<%= f.input :hero, as: :scrapbook %>
```

### Front end

```erb
<%= scrapbook_image_tag @page.hero %>

<%= @page.hero.file_url %>
```

## Contributing

### Dummy app
Scrapbook includes a dummy app for testing and development. It is located at
`spec/dummy`

Database is set up like a normal Rails app. Also run it with in the `TEST`
environment just to be safe.

```bash
$ rails db:create db:migrate
$ rails db:create db:migrate env=TEST
```

Boot the dummy app with:

```bash
$ cd spec/dummy
$ rails s
```


### Testing

In the base directory.

```bash
$ rspec
````

Coverage reports are available at `coverage/index.html`

