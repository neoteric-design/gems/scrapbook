0.6.0
=====

* Add Ruby 3.0 compatibility
* Remove Ruby <= 2.6 compatibility

0.5.2
=====

* Update TinyMCE editor to 5.x to avoid deprecation

0.5.1
=====

* Fix regression in performance tweaks

0.5.0
======

* Final feature release
* Add storehaus exporter

0.4.13
======

* Allow browsing library without being in placing? mode

0.4.12
======

* More fixes around blank assets and scrapbook_image_tag. Default to rendering
  nothing instead of exploding if asset missing.

0.4.11
======

* Fix checking for retina sized images in srcset

0.4.10
======

* Add srcset attribute for retina images in `scrapbook_image_tag` helper

0.4.9
=====

* Fix typo in install generator

0.4.8
=====

* Add active_admin generator for asset import

0.4.7
=====

* Clear for use with Rails 5.1
* Touch parent object when updating placements, to bust parent fragment cache

0.4.6
=====

* Add preset ratios so users can be consistent while 'freehand' cropping

0.4.5
=====

* Added config option `paperclip_processors`. Opens the door for adding things
  like PaperclipOptimizer, for compressing images

0.4.4
=====

* Fixed: Widget placement ids filter returning placement objects instead of a map of ids

0.4.3
=====

* Fixed: Clean up message listeners after placement data received from library popup

0.4.2
=====

* Fixed: VueJS widgets being set up with refs to same placeholder data array
  instead of a fresh copy of said data.

0.4.1
=====

* Style Changes

0.4.0
=====

* Replace parents widget with VueJS component
  * New widget init functions to match
* Add support for managing context.many attachments
* Tweaks for upload UI

0.3.4
=====

* Add initial support for rich editing captions and other fields (using TinyMCE)


0.3.3
=====

* Add `all_metadata` feature to masters and placements, which doesn't ignore blank fields

0.3.2
=====

* Store the resolution of master images upon save, because Paperclip can't
  automatically retrieve it from an S3 asset. And it'd be slow if it did!

0.3.1
=====

* Add public helpers for displaying placements (`scrapbook_image_tag`)
* More language cleanup

0.3.0
=====

* Add some actual design/styling
* Various hint texts added
* Multiple bugfixes for parent to placement associations
* Bugfix for strong parameters and metadata fields
* Lots more tiny things!

0.2.0
=====

* Scrap metadata json in favor of traditional columns

0.1.8
=====

* Add basic, basic keyword search over masters
* Add masters search form

0.1.7
=====

* Minor CSS tweaks on widget
* Fix master delete link

0.1.6
=====

* Widget beautification

0.1.0
=====

* Initial work
