# Scrapbook Design Doc

# Goals

## Version 1

* Associate an image or an album of images to any model
* Store editor provided metadata of images
* Provide cropping functionality
* Expose image sizing settings; allow customization on a per ‘context’ basis
* Support multiple sizings on pages (Retina)

# Models & Concepts

* Parent
Any record that uploads are to be attached to.

* Master
Model of an upload. Stores the original file, and master record of metadata.

* Placement
The association between a Master and a Parent. Stores the properly cropped and
sized (defined by the Context of the relation) version of a master. Also can
store a metadata diff related to this specific usage (overrides Master
metadata fields).

* Context
A user (developer) defined association classification between the Master and
its’ Parent. A Parent can have any number of images and albums by defining
multiple contexts. E.g A `User` may have an `avatar` image, a header
`hero_image`, and an album of `profile_photos` [TODO: come up with better
example]

# Metadata

Because the useful metadata for an upload likely changes wildly project to
project, we ought to reflect this fluidity in the design. Postgres' new
support for JSON allows us to store arbitrary key-value pairs without
explicity changing the schema.

While there is support for querying and indexing this data within Rails, for
performance and ease of working with them, if there are attributes that are
reasonably universal, like a title, they should be identified and broken out
into their own proper columns.

# Non-image files

The basic workflow can also play nice with non-image files. A likely solution
is to give Contexts a field to distinguish if they are for images or any
attached document. That would allow an easy way to adjust the UX ahead of
time, like filtering the file picker to just generic files, filtering
filetypes, adjusting size limits, and more.

# Retina
See `CROPPING.md`

# Open questions

* Universal attributes? Title, ...
* UX/visual representations of generic files
* Dealing with deletions and altering the master file