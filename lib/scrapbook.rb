require 'scrapbook/engine'
require 'scrapbook/configuration'
require 'scrapbook/version'

require 'scrapbook/registry'
require 'scrapbook/metadata_field'
require 'scrapbook/has_scrapbook'
require 'scrapbook/context'
require 'scrapbook/resolution'
require 'scrapbook/coordinate'

require 'scrapbook/exporters/storehaus_exporter'

require 'paperclip'
require 'paperclip_processors/cropped_resize'

require 'formtastic'
require 'inputs/scrapbook_input'

require 'bourbon'
require 'neat'
require 'jquery-rails'

module Scrapbook
end
