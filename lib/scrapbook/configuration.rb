module Scrapbook
  class << self
    attr_reader :config

    def configure
      @config = Configuration.new
      yield config
    end
  end

  class Configuration
    SETTINGS = [:metadata_fields, :contexts, :before_action, :paperclip_processors]

    attr_accessor(*SETTINGS)

    def initialize
      @metadata_fields = HashWithIndifferentAccess.new
      @contexts = HashWithIndifferentAccess.new
      @paperclip_processors = [:cropped_resize]
    end

    def register_field(...)
      field = MetadataField.new(...)
      @metadata_fields[field.name] = field
    end

    def register_context(...)
      context = Context.new(...)
      @contexts[context.name] = context
    end
  end
end
