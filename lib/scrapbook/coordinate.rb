module Scrapbook
  class Coordinate
    attr_accessor :x, :y
    def initialize(x, y)
      @x, @y = x.to_i, y.to_i
    end

    def to_a
      [x, y]
    end

    def to_s
      "#{x},#{y}"
    end

    class Type < ActiveRecord::Type::Value
      def cast(value)
        return nil unless value
        if value.is_a? Array
          Scrapbook::Coordinate.new(*value)
        elsif value.is_a? Scrapbook::Coordinate
          value
        else
          Scrapbook::Coordinate.new(*(value.split(',')))
        end
      end

      def serialize(value)
        value.to_s
      end
    end
  end
end
