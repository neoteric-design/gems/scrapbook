module Scrapbook
  module Registry
    def self.included(base)
      base.include InstanceMethods
      base.extend ClassMethods
    end

    module InstanceMethods
      def register
        self.class.register self
      end
    end

    module ClassMethods
      def registry
        fail "Please define class method `registry` to use this module"
      end

      def find(name)
        result = registry[name]
        if result.nil?
          fail "#{self.class.name} '#{name}' Not Registered"
        end
        result
      end

      def register(field)
        registry[field.name] = field
      end
    end
  end
end
