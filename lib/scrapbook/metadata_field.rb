module Scrapbook
  class MetadataField
    include Registry

    def self.registry
      Scrapbook.config.metadata_fields
    end

    attr_reader :name, :form_type

    def initialize(name, form_type = :text_field, label: nil, rte: nil)
      @name = name
      @form_type = form_type
      @label = label
      @rte = rte
    end

    def label
      @label || @name.to_s.titleize
    end

    def rte
      @rte || form_type == :text_area
    end
  end
end
