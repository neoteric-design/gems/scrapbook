module Scrapbook
  class Engine < ::Rails::Engine
    isolate_namespace Scrapbook

    config.generators do |g|
      g.test_framework :rspec, :fixture => false
    end

    initializer "scrapbook.assets.precompile" do |app|
      app.config.assets.precompile += %w(scrapbook/placeholder.png
                                         scrapbook/application.css
                                         scrapbook/application.js)
    end

    config.to_prepare do
      ActiveRecord::Type.register(:scrapbook_resolution, Scrapbook::Resolution::Type)
      ActiveRecord::Type.register(:scrapbook_coordinate, Scrapbook::Coordinate::Type)

      ActionController::Base.helper(Scrapbook::PublicHelper)
      ActiveRecord::Base.include Scrapbook::HasScrapbook
    end
  end
end
