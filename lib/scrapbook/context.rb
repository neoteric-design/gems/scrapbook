require_relative 'resolution'

module Scrapbook
  # Define the relationship between a placement and its Parent object
  class Context
    include Registry

    def self.registry
      Scrapbook.config.contexts
    end

    attr_accessor :name, :many, :target_resolution, :allow_generics

    def initialize(name, many: false, allow_generics: false,
                   target_resolution: nil)
      @name = name
      @many = many
      @allow_generics = allow_generics

      if !allow_generics && target_resolution.nil?
        raise ArgumentError, 'Image contexts must supply a target_resolution'
      end

      self.target_resolution = target_resolution
    end

    def id_accessor
      postfix = many ? '_ids' : '_id'
      "#{name.to_s.singularize}#{postfix}"
    end

    def param_name
      postfix = many ? '[]' : ''
      "[#{id_accessor}]#{postfix}"
    end

    def to_s
      name.to_s
    end

    def title
      name.to_s.titleize
    end

    def full_title
      "#{title} #{file_type_category}"
    end

    def target_resolution=(value)
      @target_resolution = Resolution::Type.new.cast(value)
    end

    def file_type_category
      allow_generics ? 'File' : 'Image'
    end
  end
end
