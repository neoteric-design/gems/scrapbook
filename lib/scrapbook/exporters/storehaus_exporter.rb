module Scrapbook
  class StorehausExporter
    def export(masters = all_masters)
      masters.find_each do |master|
        export_one(master)
      end
    end

    def export_one(master)
      blob = create_blob(master)
      puts "BLOB DID NOT SAVE: #{blob}" unless blob.persisted?
      item = create_storehaus_item(master, blob)
      master.placements.where.not(parent: nil).each do |placement|
        create_attachment(placement, blob)
      end
    end

    def create_blob(master)
      ActiveStorage::Blob.create_after_upload! io: open(master.file.url(:original)),
                                               filename: master.file_file_name,
                                               content_type: master.file_content_type
    end

    def create_storehaus_item(master, blob)
      blob.create_storehaus_item(library_metadata: master.metadata.merge(title: master.title))
    end

    def create_attachment(placement, blob)
      ActiveStorage::Attachment.new.tap do |attach|
        attach.blob = blob
        attach.record_id = placement.parent_id
        attach.record_type = placement.parent_type
        attach.name = "#{placement.context_name}"
        attach.storehaus_transformation = placement_transformation(placement)
      end.save!(touch: false)
    end

    def placement_transformation(placement)
      {
        crop: {
          x: placement.crop_origin.x,
          y: placement.crop_origin.y,
          w: placement.crop_resolution.width,
          h: placement.crop_resolution.height
        }
      }
    end

    private

    def check_storehaus_metadata_schema!
      unless scrapbook_metadata_fields == storehaus_metadata_fields
        raise "Warning: Metadata mismatch"
      end
    end

    def all_masters
      Scrapbook::Master.where.not(file_file_name: nil)
    end

    def scrapbook_metadata_fields
      Scrapbook.config.metadata_fields.keys
    end

    def storehaus_metadata_fields
      ::Storehaus::LibraryMetadata.attribute_types.keys
    end
  end
end
