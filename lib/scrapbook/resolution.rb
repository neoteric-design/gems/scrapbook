module Scrapbook
  # Value object for image dimensions
  class Resolution
    RETINA_THRESHOLD = 1.8
    RETINA_FACTOR = 2

    attr_accessor :width, :height
    def initialize(width, height = 0)
      @width = width.to_i
      raise ArgumentError 'Width must be greater than zero' unless @width > 0
      @height = height.to_i
    end

    def aspect_ratio
      return nil if width_only?
      width.to_f / height.to_f
    end

    def ==(other)
      width == other.width && height == other.height
    end

    def to_s
      "#{width}x#{height}"
    end

    def retinable?(other)
      if width_only? || other.width_only?
        width_retinable?(other.width)
      else
        width_retinable?(other.width) && height_retinable?(other.height)
      end
    end

    def retinafy
      self.class.new(width * retina_factor, height * retina_factor)
    end

    def retina_threshold
      RETINA_THRESHOLD
    end

    def retina_factor
      RETINA_FACTOR
    end

    def width_only?
      height.zero?
    end

    # Cast a resolution object from db/array/string
    class Type < ActiveRecord::Type::Value
      def cast(value)
        if value.is_a? Scrapbook::Resolution
          value
        elsif value.is_a? Array
          Scrapbook::Resolution.new(*value)
        elsif value.is_a?(String) && value.match(/\dx\d/)
          Scrapbook::Resolution.new(*value.split('x'))
        end
      end

      def serialize(value)
        value.to_s
      end
    end

    private

    def width_retinable?(other_width)
      (width / other_width) >= retina_threshold
    end

    def height_retinable?(other_height)
      (height / other_height) >= retina_threshold
    end
  end
end
