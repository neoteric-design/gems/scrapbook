module Scrapbook
  module HasScrapbook
    def self.included(klass)
      klass.extend ClassMethods
    end

    module ClassMethods
      def has_scrapbook(context_name)
        context = Context.find(context_name)
        has_many :scrapbook_placements
        if context.many
          has_many context.name, -> { context(context.name) },
                   foreign_key: :parent_id,
                   class_name: '::Scrapbook::Placement',
                   as: :parent,
                   autosave: true,
                   dependent: :destroy

          define_method "#{context.id_accessor}=" do |other_ids|
            other_ids.compact.each_with_index do |other_id, idx|
              ::Scrapbook::Placement.where(id: other_id).update_all(position: idx)
            end
            super(other_ids)
          end
        else
          has_one context.name, -> { context(context.name) },
                  foreign_key: :parent_id,
                  class_name: '::Scrapbook::Placement',
                  as: :parent,
                  autosave: true,
                  dependent: :destroy

          define_method context.id_accessor do
            send(context.name)&.id
          end

          define_method "#{context.id_accessor}=" do |other_id|
            other = ::Scrapbook::Placement.find_by(id: other_id)
            send("#{context.name}=", other)
          end
        end
      end
    end
  end
end
