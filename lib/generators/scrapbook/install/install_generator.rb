module Scrapbook
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def copy_templates
      template "initializer.rb.erb",
               "config/initializers/scrapbook.rb"
    end

    def setup_routes
      route "mount Scrapbook::Engine => '/scrapbook/'"
    end

    def install_migrations
      rake 'scrapbook:install:migrations'
    end

    def generate_admin
      generate 'scrapbook:active_admin' if defined?(ActiveAdmin)
    end
  end
end
