module Paperclip
  class CroppedResize < Thumbnail
    def initialize(file, options = {}, attachment = nil)
      super
      @current_geometry.width = target.crop_metrics[:width]
      @current_geometry.height = target.crop_metrics[:height]
    end

    def target
      @attachment.instance
    end

    def transformation_command
      crop_command = [
         "-crop",
         "#{target.crop_metrics[:width]}x" \
         "#{target.crop_metrics[:height]}+" \
         "#{target.crop_metrics[:x]}+" \
         "#{target.crop_metrics[:y]}",
         "+repage"
      ]

      crop_command + super
    end
  end
end
