namespace :scrapbook do
  desc "Updates records after a context is renamed."
  task :rename_context, [:old_name, :new_name] => :environment do |t, args|
    unless args[:old_name] && args[:new_name]
      abort "Please specify the old and new context names\n" +
            "rake scrapbook:rename_context[my_old_name, new_better_name]"
    end

    placements = Scrapbook::Placement.where(context_name: args[:old_name])
    puts "Found #{placements.count} placements under '#{args[:old_name]}'."

    abort unless placements.any?

    puts "Updating..."
    placements.update_all(context_name: args[:new_name])
  end
end
