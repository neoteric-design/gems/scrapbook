class ScrapbookInput < Formtastic::Inputs::StringInput
  def widget
    template.content_tag('scrapbook-widget', '',
                  'v-bind:placeholder-path': "'#{placeholder_path}'",
                  'v-bind:library-path': "'#{library_path}'",
                  'v-bind:context': "'#{context.name}'",
                  'v-bind:fieldname': "'#{field_name}'",
                  'v-bind:many': "#{context.many}",
                  'v-bind:initial-placements': "#{placements_data}")
  end


  def to_html
    input_wrapping do
      label_html <<
      widget
    end
  end

  private

  def placeholder_path
    options.fetch(:placeholder_path, default_placeholder)
  end

  def default_placeholder
    template.image_path('scrapbook/placeholder.png')
  end

  def library_path
    Scrapbook::Engine.routes.url_for controller: '/scrapbook/masters',
                                     action: :index,
                                     only_path: true
  end

  def placements
    object.try(method)
  end

  def placements_data
    [placements].reject(&:nil?).flatten.map(&:to_data).to_json
  end

  def context
    Scrapbook::Context.find(method)
  end

  def field_name
    "#{object_name}#{context.param_name}"
  end
end
