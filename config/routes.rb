Scrapbook::Engine.routes.draw do
  resources :masters do
    collection do
      get :search
    end
  end
  resources :placements
end
