class CreateScrapbookPlacements < ActiveRecord::Migration[5.0]
  def change
    create_table :scrapbook_placements do |t|
      t.belongs_to :scrapbook_master, foreign_key: true
      t.belongs_to :parent, polymorphic: true

      t.integer :position
      t.string :context_name
      t.string :crop_origin
      t.string :crop_resolution
      t.attachment :file

      # Metadata
      t.string :alt_text
      t.string :copyright
      t.string :source
      t.text :caption
      t.text :notes
      t.datetime :taken_on

      t.timestamps
    end
  end
end
