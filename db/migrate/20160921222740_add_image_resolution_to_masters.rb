class AddImageResolutionToMasters < ActiveRecord::Migration[5.0]
  def change
    add_column :scrapbook_masters, :image_resolution, :string
  end
end
