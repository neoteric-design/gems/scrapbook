class CreateScrapbookMasters < ActiveRecord::Migration[5.0]
  def change
    create_table :scrapbook_masters do |t|
      t.attachment :file
      t.string :title

      # Metadata
      t.string :alt_text
      t.string :copyright
      t.string :source
      t.text :caption
      t.text :notes
      t.datetime :taken_on

      t.timestamps
    end
  end
end
