require 'rails_helper'

module Scrapbook
  RSpec.describe Placement, type: :model do
    let(:master) { Master.new }
    let(:placement) do
      described_class.new(master: master, context_name: 'dummy1')
    end

    before(:all) do
      Context.new('dummy1', target_resolution: '600x600').register
    end

    it 'has a scrapbook asset' do
      expect(placement.build_master).to be_a(Scrapbook::Master)
    end

    it 'has some kinda parent' do
      expect(placement).to respond_to(:parent, :parent_id, :parent_type)
    end

    describe 'copies the file from master' do
      let(:master) do
        Master.new(file: File.new(file_fixture('test-image-a.jpg')))
      end

      let(:placement) { described_class.new(context_name: 'dummy1') }
      it 'when assigned directly' do
        placement.master = master

        expect(placement.file_file_name).to eq(master.file_file_name)
      end

      it 'when assigned via master' do
        master.placements = [placement]

        expect(placement.file_file_name).to eq(master.file_file_name)
      end

      it 'when assigned via master_id' do
        master.save
        placement.scrapbook_master_id = master.id

        expect(placement.file_file_name).to eq(master.file_file_name)
      end

      it 'when assigned via initialization attributes' do
        master.save
        placement = Placement.new(scrapbook_master_id: master.id,
                                  context_name: 'dummy1')

        expect(placement.file_file_name).to eq(master.file_file_name)
      end
    end

    describe 'cropping' do
      it 'has a width and height for the crop selection' do
        expect(placement).to respond_to :crop_resolution
      end

      it 'has a coordinate set for the crop top left corner' do
        expect(placement).to respond_to :crop_origin
      end

      it 'crop origin accepts x and y values' do
        placement.crop_origin = [200, 300]
        expect(placement.crop_origin.x).to eq(200)
      end

      it 'crop origin defaults to 0,0' do
        expect(described_class.new.crop_origin.to_s).to eq('0,0')
      end
    end

    describe 'sizes' do
      it 'has a standard that is equal to the target res' do
        expect(placement.sizes).to include(standard: '600x600')
      end

      context 'when crop isnt big enough for retina' do
        before(:each) { placement.crop_resolution = '200x200' }

        it do
          expect(placement.crop_retina_ready?).to be_falsey
        end

        it 'does not have a retina in sizes' do
          expect(placement.sizes.keys).to_not include(:retina)
        end
      end

      context 'when crop is large enough for a retina' do
        before(:each) { placement.crop_resolution = '2000x2000' }

        it do
          expect(placement.crop_retina_ready?).to be_truthy
        end

        it 'adds a retina to sizes' do
          expect(placement.sizes).to include(retina: '1200x1200')
        end
      end
    end

    describe 'metadata' do
      let(:master_metadata) do
        { 'caption' => 'Stuff', 'taken_on' => Date.today.to_s }
      end
      let(:master) { Master.new(metadata: master_metadata) }
      let(:overriding_metadata) { { 'caption' => 'Not stuff' } }

      it 'has its own overriding_metadata' do
        expect(placement).to respond_to :overriding_metadata
      end

      it '#metadata merges master metadata with overriding' do
        placement.master = master
        placement.overriding_metadata = overriding_metadata
        expected = master_metadata.merge(overriding_metadata)

        expect(placement.metadata).to eq(expected)
      end
    end
  end
end
