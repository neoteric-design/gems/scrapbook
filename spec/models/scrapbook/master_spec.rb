require 'rails_helper'

module Scrapbook
  RSpec.describe Master, type: :model do
    let(:master) { build(:master) }
    let(:test_file_a) { File.new(file_fixture('test-image-a.jpg')) }
    let(:test_file_b) { File.new(file_fixture('test-image-b.jpg')) }

    let(:placement_1) { build(:placement) }
    let(:placement_2) { build(:placement) }

    it 'has a paperclip attachment named file' do
      expect(master).to respond_to :file
    end

    it 'has many placements' do
      expect(master).to respond_to(:placements)
    end

    describe 'title' do
      context 'is present' do
        let(:master) { build(:master, title: "Hello") }

        it 'returns db value' do
          expect(master.title).to eq("Hello")
        end
      end

      context 'is blank' do
        let(:master) { build(:master, title: nil, file: test_file_a) }

        it 'returns file name' do
          expect(master.title).to eq(File.basename(test_file_a))
        end
      end
    end

    describe 'allowed file types' do
      pending
    end

    describe 'images cannot be updated' do
      before(:each) { master.file = test_file_a }

      it 'silently drops new sets' do
        master.save

        master.file = test_file_b

        expect(master.file_file_name).to eq('test-image-a.jpg')
      end
    end

    describe 'deletion' do
      it 'removes its placements' do
        master.placements = [placement_1, placement_2]
        master.save

        master.destroy

        expect(master.placements).to be_empty
      end
    end

    describe 'keyword_search' do
      before(:each) do
        @pyramid = Master.create(title: 'Pyramid', metadata: { notes: "Wow" })
        @machu = Master.create(title: 'Machu Picchu', metadata: { notes: "Grass Tall", source: "Joe" })
        @knoll = Master.create(title: 'Grassy Knoll', metadata: { notes: "Lush" })
      end

      it 'matches a keyword in title' do
        expect(Master.keyword_search('pyra')).to match_array([@pyramid])
        expect(Master.keyword_search('picchu')).to match_array([@machu])
      end

      it 'matches a keyword in metadata' do
        expect(Master.keyword_search('wow')).to match_array([@pyramid])
        expect(Master.keyword_search('Joe')).to match_array([@machu])
      end

      it 'matches a keyword across title and metadata' do
        expect(Master.keyword_search('grass')).to match_array([@machu, @knoll])
      end
    end

    describe 'image_resolution' do
      it 'if image, grabs the resolution of the image upon save' do
        master.file = test_file_a
        master.save!

        expect(master.image_resolution).to be_a(Scrapbook::Resolution)
      end
    end

    describe 'metadata' do
      it 'is a hash including metadata fields with values' do
       # pending
      end

      describe 'all_metadata' do
        it 'includes all fields, even blanks' do
        end
      end
    end
  end
end
