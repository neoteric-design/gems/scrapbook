require 'rails_helper'

module Scrapbook
  RSpec.feature 'Creating placement from new master' do
    scenario do
      master = create(:master, file: File.new(file_fixture('test-image-a.jpg')),
                      title: 'Dog',
                      alt_text: 'Photo of a canine',
                      caption: 'Taken in the park')

      placement = create(:placement, master: master,
                         alt_text: 'Rex from next door')

      visit scrapbook.placement_path(placement)

      expect(page).to have_content(master.title)
      expect(page).to have_content(master.caption)

      expect(page).to_not have_content(master.alt_text)
      expect(page).to have_content(placement.alt_text)
    end
  end
end
