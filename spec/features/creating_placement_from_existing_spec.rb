require 'rails_helper'

module Scrapbook
  RSpec.feature 'Creating placement from existing master' do
    scenario do
      Master.create(title: 'hello',
                    file: File.new(file_fixture('test-image-a.jpg')))

      visit scrapbook.masters_path(context: 'hero')
      click_on 'hello'

      click_on 'Create Hero Image'
      find('#placement_crop_resolution', visible: false).set "300x100"

      fill_in 'Alt Text', with: "hovering text"
      click_on 'Save Hero Image'

      expect(page).to have_content('hovering text')
    end
  end
end
