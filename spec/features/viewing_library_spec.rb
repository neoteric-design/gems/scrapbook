require 'rails_helper'

module Scrapbook
  RSpec.feature "Viewing Scrapbook Library" do
    it do
      3.times do |x|
        Master.create(title: "hello-#{x}",
                      file: File.new(file_fixture('test-image-a.jpg')))
      end

      visit scrapbook.masters_path

      Master.all.each do |master|
        expect(page).to have_link(master.title,
                                  href: scrapbook.master_path(master))
        expect(page).to_not have_link(href: /#{Regexp.escape(scrapbook.new_placement_path)}.*/)
      end
    end

    it 'view master' do
      master = Master.create(title: "hello",
                    file: File.new(file_fixture('test-image-a.jpg')))
      placement = Placement.create(master: master, context_name: 'hero')

      visit scrapbook.master_path(master)

      expect(page).to have_text("1 Placement")

      click_on "Edit"

      expect(page).to have_field("Title", with: "hello")
    end
  end
end
