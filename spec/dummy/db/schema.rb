# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160921222740) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "parents", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scrapbook_masters", id: :serial, force: :cascade do |t|
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "title"
    t.string "alt_text"
    t.string "copyright"
    t.string "source"
    t.text "caption"
    t.text "notes"
    t.datetime "taken_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_resolution"
  end

  create_table "scrapbook_placements", id: :serial, force: :cascade do |t|
    t.integer "scrapbook_master_id"
    t.string "parent_type"
    t.integer "parent_id"
    t.integer "position"
    t.string "context_name"
    t.string "crop_origin"
    t.string "crop_resolution"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "alt_text"
    t.string "copyright"
    t.string "source"
    t.text "caption"
    t.text "notes"
    t.datetime "taken_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parent_type", "parent_id"], name: "index_scrapbook_placements_on_parent_type_and_parent_id"
    t.index ["scrapbook_master_id"], name: "index_scrapbook_placements_on_scrapbook_master_id"
  end

  add_foreign_key "scrapbook_placements", "scrapbook_masters"
end
