Scrapbook.configure do |config|
  config.register_context :hero, many: false, target_resolution: [600, 200]
  config.register_context :freestyle, many: false, target_resolution: [600, 0]
  config.register_context :gallery, many: true, target_resolution: [800, 200]

  config.register_field :alt_text
  config.register_field :copyright
  config.register_field :source
  config.register_field :caption, :text_area
  config.register_field :notes, :text_area
  config.register_field :taken_on, :datetime_field
end
