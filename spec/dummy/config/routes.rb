Rails.application.routes.draw do
  mount Scrapbook::Engine => "/scrapbook"

  resources :parents
end
