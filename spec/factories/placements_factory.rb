FactoryGirl.define do
  factory :placement, class: Scrapbook::Placement do
    context_name 'hero'
    master
  end
end
