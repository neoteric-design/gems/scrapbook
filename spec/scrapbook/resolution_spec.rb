require 'spec_helper'

module Scrapbook
  RSpec.describe Resolution do
    let(:res) { Resolution.new(600, 200) }
    it 'has a width and height' do
      expect(res).to respond_to :width, :height
    end

    describe 'equality' do
      it 'equal when width and height are the same' do
        one = Resolution.new(100, 200)
        two = Resolution.new(100, 200)
        three = Resolution.new(200, 100)

        expect(one == two).to be_truthy
        expect(one == three).to be_falsey
      end
    end

    describe '#to_s' do
      it 'outputs width x height' do
        res.width = 500; res.height = 200

        expect(res.to_s).to eq("500x200")
      end
    end

    describe '#aspect_ratio' do
      it 'equals width divided by height' do
        res.width = 400; res.height = 200

        expect(res.aspect_ratio).to eq(2)
      end
    end

    describe '#retinable?' do
      let(:target) { Resolution.new(500, 300) }
      let(:small) { Resolution.new(600, 400) }
      let(:wide_short) { Resolution.new(1500, 400) }
      let(:narrow_tall) { Resolution.new(600, 1000) }
      let(:large) { Resolution.new(1500,900) }

      context 'when self is large enough to be a workable retina of given res' do
        it 'is true' do
          expect(large.retinable?(target)).to be_truthy
        end
      end

      context 'when not large enough to be a workable retina' do
        it 'is false' do
          expect(small.retinable?(target)).to be_falsey
          expect(wide_short.retinable?(target)).to be_falsey
          expect(narrow_tall.retinable?(target)).to be_falsey
        end
      end
    end

    describe '#retinafy' do
      let(:stub_factor) { 2 }
      it 'returns a new resolution object with dimensions multiplied by retina factor' do
        allow(res).to receive(:retina_factor).and_return(stub_factor)
        expected = Resolution.new(res.width * stub_factor,
                                  res.height * stub_factor)
        expect(res.retinafy).to eq(expected)
      end
    end

    context 'width only (height == 0)' do
      let(:res) { Resolution.new(600) }

      describe '#retinable?' do
        let(:small) { Resolution.new(700, 400) }
        let(:narrow_tall) { Resolution.new(700, 1000) }
        let(:large) { Resolution.new(1200, 2000) }

        it 'only checks width' do
          expect(small.retinable?(res)).to be_falsey
          expect(narrow_tall.retinable?(res)).to be_falsey
          expect(large.retinable?(res)).to be_truthy
        end
      end
    end
  end

end