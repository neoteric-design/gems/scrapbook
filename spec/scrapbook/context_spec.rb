require 'rails_helper'

RSpec.describe Scrapbook::Context do
  let(:context) { described_class.new(:hero, target_resolution: [200, 200]) }

  it 'has a name' do
    expect(context).to respond_to :name
  end

  it 'has a signal for if it represents one image or a collection' do
    expect(context).to respond_to :many
  end

  it 'can represent images or any file' do
    expect(context).to respond_to :allow_generics
  end

  it 'has a target resolution' do
    expect(context).to respond_to :target_resolution
  end

  describe 'new' do
    context 'allow_generics: false' do
      it 'does require a target_resolution' do
        expect do
          described_class.new(:test, allow_generics: false)
        end.to raise_error(ArgumentError)
      end
    end

    context 'allow_generics: true' do
      it 'does not require a target_resolution' do
        expect do
          described_class.new(:test, allow_generics: true)
        end.to_not raise_error
      end
    end
  end

  describe 'registry' do
    it 'keeps track of registered contexts' do
      context.register
      expect(Scrapbook::Context.find(:hero)).to eq(context)
    end
  end

  describe 'title' do
    it 'presents a titleized version of the name' do
      expect(context.title).to eq('Hero')
    end
  end

  describe 'file_type_category returns a noun for the general type of file' do
    context 'allow_generics is false' do
      let(:context) do
        described_class.new(:definitely_image, allow_generics: false,
                                               target_resolution: [100, 100])
      end

      it 'returns "Image"' do
        expect(context.file_type_category).to eq("Image")
      end
    end

    context 'allow_generics is true' do
      let(:context) do
        described_class.new(:definitely_image, allow_generics: true)
      end

      it 'returns "File"' do
        expect(context.file_type_category).to eq("File")
      end
    end
  end

  describe 'target_resolution' do
    let(:dimensions) { [500, 300] }
    let(:equivalent_resolution) { Scrapbook::Resolution.new(*dimensions) }

    it 'can be assigned as an array pair' do
      context.target_resolution = dimensions

      expect(context.target_resolution).to eq(equivalent_resolution)
    end

    it 'can be assigned as a resolution obj directly' do
      context.target_resolution = Scrapbook::Resolution.new(*dimensions)

      expect(context.target_resolution).to eq(equivalent_resolution)
    end
  end

  describe 'id_accessor reflects natural rails association id method name' do
    context 'when many: false' do
      let(:context) do
        described_class.new(:definitely_image, many: false,
                                               target_resolution: [100, 100])
      end

      it 'returns name + "_id"' do
        expect(context.id_accessor).to eq("definitely_image_id")
      end
    end
    context 'when many: true' do
      let(:context) do
        described_class.new(:gallery_images, many: true,
                                             target_resolution: [100, 100])
      end

      it 'returns singularized name + "_ids"' do
        expect(context.id_accessor).to eq("gallery_image_ids")
      end
    end
  end
end
