require 'rails_helper'

RSpec.describe Scrapbook::HasScrapbook do
  it 'makes has_scrapbook available to models' do
    expect(Parent).to respond_to(:has_scrapbook)
  end

  describe '#has_scrapbook' do
    let(:parent) { Parent.new }
    let(:hero_1) { create(:placement) }
    let(:hero_2) { create(:placement) }
    let(:gallery_1) { create(:placement, context_name: 'gallery') }
    let(:gallery_2) { create(:placement, context_name: 'gallery') }

    context 'many: true' do
      it 'sets up has_many relation to placements' do
        expect(parent).to respond_to(:gallery)
        expect(parent.gallery).to be_a ::ActiveRecord::Relation
      end

      it 'takes an id list' do
        parent.gallery_ids = [gallery_2.id, gallery_1.id]
        parent.save!

        expect(parent.gallery).to include(gallery_2, gallery_1)
        expect(parent.gallery.size).to eq(2)
        expect(gallery_1.reload.parent).to eq(parent)
      end
    end

    context 'many: false' do
      let(:placement) { create(:placement) }

      it 'sets up has_one relation to placements' do
        expect(parent).to respond_to(:hero)
        expect(parent.hero).to be_nil
      end

      describe 'assigning' do
        it 'has an ID accessor for relation' do
          parent.hero_id = placement.id
          expect(parent.hero).to eq(placement)
          expect(parent.hero_id).to eq(placement.id)
        end

        it 'sets nil if the given id is nil' do
          parent.hero_id = nil
          expect(parent.hero).to be_nil
        end

        it 'sets nil if the given id is empty' do
          parent.hero_id = ''
          expect(parent.hero).to be_nil
        end
      end
    end

    describe 'replacement and removal' do
      context 'many: false' do


        it 'when saving a parent, placements that were removed or replaced from that parent are deleted' do
          parent.hero = hero_1
          parent.save!

          parent.hero = hero_2
          parent.save!

          expect(hero_1).to be_destroyed
        end
      end

      context 'many: true' do
        it 'when saving a parent, placements that were removed or replaced from that parent are deleted' do
          parent.gallery = [gallery_1, gallery_2]
          parent.save!

          parent.gallery = [gallery_2]
          parent.save!

          expect(gallery_1).to be_destroyed
        end
      end
    end
  end
end