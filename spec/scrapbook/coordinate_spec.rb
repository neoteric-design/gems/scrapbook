require 'spec_helper'

module Scrapbook
  RSpec.describe Coordinate do
    let(:coordinate) { Coordinate.new(250, 300) }

    it 'has x and y' do
      expect(coordinate).to respond_to(:x, :y)
    end

    describe "to_s" do
      it { expect(coordinate.to_s).to eq('250,300') }
    end

    describe "to_a" do
      it { expect(coordinate.to_a).to eq([250, 300]) }
    end
  end
end